CC = clang++-3.6

FLAGS = -std=c++14 -g3

INCLUDE = /usr/include/c++/4.8/ 

LIBS = unicorn

main: src/include/ElfReader.h src/main.cpp src/include/ElfEmulator.h src/include/x86Emulator.h src/include/x64Emulator.h src/include/mipsEmulator.h src/include/armEmulator.h
	${CC} ${FLAGS} -o main src/main.cpp -l ${LIBS} #-l glib-2.0 -lpthread --static

