#include "include/ElfReader.h"
#include "include/Emulators.h"
#include <string.h>
#include <iostream>




template<typename T>
void process(std::shared_ptr<EMU::ElfEmulator<T>> emu){
	emu->Load();
	emu->SetAddr();
	emu->SetSyscallHook();
	//emu->SetTraceHook();
	emu->Run();
	emu->PrintCPUCont();
}

int main(int argc, char ** argv){
	if(argc < 2){
		std::cout << "Nope\n";
		return 2;
	}
	std::ifstream f(argv[1]);
	char bits;
	f.seekg(4);
	f.read(&bits, 1);
	if(bits == 2){
		auto emu = get64Emu(std::make_shared<ELF::ElfReader<uint64_t>>(argv[1]));
		process(emu);
	}else{
		auto emu = get32Emu(std::make_shared<ELF::ElfReader<uint32_t>>(argv[1]));
		process(emu);
	}
	return 1;
/*
	reader->LoadFile(false);
	EMU::mipsEmulator emulator(reader);
	emulator.Load();
	std::cout << std::hex << reader->GetEntry()<< "\n";
	emulator.SetAddr(reader->GetEntry());	
	emulator.SetSyscallHook();
//	emulator.SetTraceHook();
	emulator.Run();
*/
}
