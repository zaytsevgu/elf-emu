#ifndef ELF_EMU_gMIPS

#define ELF_EMU_gMIPS

#include <iomanip>
#include "ElfReader.h"
#include "ElfEmulator.h"
#include <unicorn/unicorn.h>

namespace EMU{

	class mipsEmulator: public ElfEmulator<uint32_t> {

		public:
			mipsEmulator(std::shared_ptr<ELF::ElfReader<uint32_t>> r):ElfEmulator<uint32_t>(r){};
		
		protected:
			
			static void trace_hook(uc_engine *uc, uint32_t address, uint32_t size, void *user_data){
			}

			void Dump() override{
				/**
                 * UGLY DUMPER
                 **/
				std::cout << "!!!! RUNNING UGLY DUMPER !!!!\n";
                std::ofstream f("out_dump");
                uint16_t phsize;
                uint32_t phoffset;
                uc_mem_read(this->unicorn, this->addrs+0x2c, &phsize, sizeof(phsize));
                uc_mem_read(this->unicorn, addrs+0x1c, &phoffset, sizeof(phoffset));
                uint32_t addr = addrs + be_read(phoffset);
                for(uint32_t i = 0; i < be_read(phsize); ++i){
					uint32_t val;
					uint32_t type;
					uc_mem_read(this->unicorn, addr, &type, sizeof(type));
                    if(be_read(type) == 1){ 
                    	uc_mem_read(this->unicorn, addr + 4, &val, sizeof(val));
                        f.seekp(be_read(val));
                        uc_mem_read(this->unicorn, addr + 8, &val, sizeof(val));
                        uint32_t size;
                        uc_mem_read(this->unicorn, addr+ 0x10, &size, sizeof(size));
						size = be_read(size);
						val = be_read(val);
                        auto data = std::make_unique<char[]>(size);
                        uc_mem_read(this->unicorn, val, data.get(), size);
                        f.write(data.get(), size);
                    }   
                 	addr += reader->GetPhsize();
                }   
                f.close();
                std::cout << "!!!! OK !!!!\n";
			}

			static void syscall_hook(uc_engine *uc, void *garbage, void *user_data)
			{
				uint32_t syscall_num;
				uint32_t arg1;
				uint32_t arg2;
				uint32_t arg3;
				uint32_t arg4;
				uint32_t ret = 42;
				mipsEmulator *ptr = static_cast<mipsEmulator *>(user_data);
				uc_reg_read(uc, UC_MIPS_REG_2, &syscall_num);
				uc_reg_read(uc, UC_MIPS_REG_4, &arg1);
				uc_reg_read(uc, UC_MIPS_REG_5, &arg2);
				uc_reg_read(uc, UC_MIPS_REG_6, &arg3);
				uc_reg_read(uc, UC_MIPS_REG_7, &arg4);
				switch (syscall_num){
					case 0xfa1: {	
						std::cout << "SYS_EXIT|" << arg1 << "\n";
						uc_emu_stop(uc);
						break;
					}
					case 0xfcd: {
						std::cout << "SYS_BRK|" << arg1 <<"\n";
						ptr->Dump();
					}
					case 0xffa: {
							std::cout << std::hex << "SYS_MMAP|" << arg1 <<"|" << arg2 <<"|" <<arg3 <<"|" << arg4 << "\n";
//							uc_mem_read(uc, arg1+4, &arg2, sizeof(arg2));
//							uc_mem_read(uc, arg1+8, &arg3, sizeof(arg3));
//							uc_mem_read(uc, arg1, &arg1, sizeof(arg1));
							uc_err err = uc_mem_map(uc, arg1, ptr->getAligned(arg2), 7);
							if(err){
								err = uc_mem_unmap(uc, arg1, arg2);
                            	if(err) {
                                	std::cerr << ">>>>>>> Unmmap: " << uc_strerror(err) << "\n";
									auto pair_mmap = ptr->maps.cbegin();
									do{
                                		pair_mmap = std::find_if(ptr->maps.cbegin(), ptr->maps.cend(), [arg1,arg2](std::pair<uint32_t, uint32_t> x){
                                        	return (((x.first <= arg1) && (x.first+x.second > arg1))||((x.first >= arg1)&&(x.first < arg1 + arg2)));
                                    	});
                                		if(pair_mmap != ptr->maps.cend()){
											std::cout << pair_mmap->first << " " << pair_mmap->second << "\n";
                                    		err = uc_mem_unmap(uc, pair_mmap->first, pair_mmap->second);
                                    		std::cerr << ">>>>>>> Unmmap2: " << uc_strerror(err) << "\n";
											ptr->maps.erase(pair_mmap);
                                		}else{
											break;
										}
									}while(!err);
                            	}
                            	err = uc_mem_map(uc, arg1, ptr->getAligned(arg2), arg3);
                            	std::cerr << ">>>>>>> Mmap: " << uc_strerror(err) << "\n";
							}
							if(!err){
        	                    ptr->maps.push_back(std::pair<uint32_t, uint32_t>(arg1, ptr->getAligned(arg2)));
            	                uc_reg_write(uc, UC_MIPS_REG_2, &arg1);
								ret = 0;
            	                uc_reg_write(uc, UC_MIPS_REG_7, &ret);
                	        }else{
								ret = -2;	
            	                uc_reg_write(uc, UC_MIPS_REG_2, &ret);
								ret = 1;
            	                uc_reg_write(uc, UC_MIPS_REG_7, &ret);
							}
							break;
					}
					case 0xff5: {
							std::cout << "SYS_READLINK|" << arg1 << "|" << arg2 << "|" << arg3<<  "\n";
							ret = 0;
            	            uc_reg_write(uc, UC_MIPS_REG_7, &ret);
							ret = 42;
							uc_reg_write(uc, UC_MIPS_REG_2, &ret);
							break;
					}
					case 0x1033: {
							std::cout << "SYS_CACHEFLUSH|" << arg1 << "|" << arg2 << "|" << arg3 <<"\n";
							ret = 0;
            	            uc_reg_write(uc, UC_MIPS_REG_7, &ret);
							uc_reg_write(uc, UC_MIPS_REG_2, &ret);
							break;
					}
					case 0x101d:{
							std::cout << "SYS_MPROTECT|" << arg1 <<"|" << arg2 <<"|" <<arg3 <<"|" << arg4 << "\n";
							ret = 0;
							if(!ptr->addrs)
								ptr->addrs = arg1;
							uc_reg_write(uc, UC_MIPS_REG_2, &ret);
            	            uc_reg_write(uc, UC_MIPS_REG_7, &ret);
							break;
					}
					default: {
							std::cout << "Got "<< syscall_num<< " syscall\n";
							std::cout << "Now I'm exit\n";
							uc_emu_stop(uc);
						}
				}
			}

			uc_arch getArch() override{
				return UC_ARCH_MIPS;
			}

			uc_mode getSize() override{
				return (uc_mode)(4 | (reader->isBE() << 30));
			}

			uint32_t getAligned(uint32_t size) override{
				if (size & 0xfff){
					size = ((size >> 12)+1) << 12;
				}
				return size;
			}

			uint32_t getLowAlign(uint32_t addr) override{
				return (addr >> 16)	<< 16;
			}
			
			void setStackPointer() override{
				uint32_t addr = 0xc0000000;
				uint32_t size = 20 * 1024 * 1024;
				uc_mem_map(this->unicorn, addr-size, size, UC_PROT_READ | UC_PROT_WRITE);
				uint32_t initial_stack =  addr- 10 * 1024 * 1024;
				uc_reg_write(this->unicorn, UC_MIPS_REG_29, &initial_stack);
			}

			uint32_t be_write(uint32_t val){
				if(this->reader->isBE()){
					return htobe32(val);
				}
				return val;
			}
		
			template<typename T>
			T be_read(T val){
				if(this->reader->isBE()){
					if(sizeof(T) == 4)
						return be32toh(val);
					if(sizeof(T) == 2)
						return be16toh(val);
				}
				return val;
			}
	
			void makeAux() override{
				//T aux = 0x7ffe000000000;
				//T size = 1024;
				//uc_mem_map(this->unicorn, aux, size, UC_PROT_READ|UC_PROT_WRITE);
				uint32_t addr;
				uc_reg_read(this->unicorn, UC_MIPS_REG_29, &addr);
				uint32_t val = be_write(0);
                uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
                uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
                uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
                val = be_write(5);
                uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
                val = be_write(this->reader->GetPhnum());
                uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
                val = be_write(3);
                uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
                val = be_write(this->loadaddr + this->reader->GetPhoff());
                uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
                val = be_write(9);
                uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
                val = be_write(this->reader->GetEntry());
                uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
                val = be_write(4);
                uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
                val = be_write(this->reader->GetPhsize());
                uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
				val = be_write(6);
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));
                addr += sizeof(val);
				size_t x;
				uc_query(this->unicorn, UC_QUERY_PAGE_SIZE, &x);
				val = be_write(x);
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));
				
			}

		public:

			static void hook_code(uc_engine *uc, uint64_t address, uint32_t size, void *user_data)
			{
				mipsEmulator *s = (mipsEmulator *)(user_data);
				//s->PrintCPUCont();
			    std::cout <<std::hex<< ">>> Tracing instruction "<< address << " " << size << "\n";
				uint32_t inst;
				uc_mem_read(uc, address, &inst, sizeof(inst));
				std::cout << inst << "\n";
			}

			void SetSyscallHook() override{
				uc_hook hook;
				uc_hook_add(this->unicorn, &hook, UC_HOOK_INTR, (void *)syscall_hook, this, 1, 0, 0);
			}

			void SetTraceHook() override{
				uc_hook trace;
				uc_hook_add(this->unicorn, &trace, UC_HOOK_CODE, (void *)hook_code, this, 1, 0);
			}

			void SetAddr() override{
				uint32_t addr = this->reader->GetEntry();
				uc_reg_write(this->unicorn, UC_MIPS_REG_PC, &addr);
			}
			
			uint32_t GetAddr() override{
				uint32_t ret;
				uc_reg_read(this->unicorn, UC_MIPS_REG_PC, &ret);
				return ret;
			}	
			void PrintCPUCont() override{
				int32_t reg;
			    uc_reg_read(this->unicorn, UC_MIPS_REG_PC, &reg);
    			std::cout << ">>> RIP = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_1, &reg);
    			std::cout << ">>> R1 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_2, &reg);
    			std::cout << ">>> R2 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_3, &reg);
    			std::cout << ">>> R3 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_4, &reg);
    			std::cout << ">>> R4 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_5, &reg);
    			std::cout << ">>> R5 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_6, &reg);
    			std::cout << ">>> R6 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_7, &reg);
    			std::cout << ">>> R7 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_8, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_9, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_10, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_11, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_12, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_13, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_14, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_15, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_16, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_17, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_18, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_19, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_20, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_21, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_22, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_23, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_24, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_25, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_26, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_27, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_28, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_29, &reg);
    			std::cout << ">>> R8 = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_30, &reg);
    			std::cout << ">>> FP = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_MIPS_REG_31, &reg);
    			std::cout << ">>> RA = "<< reg << "\n";
			}
	};
}

#endif
