#ifndef ELF_EMU_g86

#define ELF_EMU_g86


#include <iomanip>
#include <iterator>
#include <algorithm>
#include "ElfReader.h"
#include "ElfEmulator.h"
#include <unicorn/unicorn.h>

namespace EMU{
	
	class x86Emulator: public ElfEmulator<uint32_t> {
		public:

			void Dump() override{
				/**
				 * UGLY DUMPER
				 **/
				std::cout << "!!!! RUNNING UGLY DUMPER !!!!\n";
				std::ofstream f("out_dump");
				uint16_t phsize;
				uint32_t phoffset;
				uc_mem_read(this->unicorn, this->addrs+0x2c, &phsize, sizeof(phsize));
				uc_mem_read(this->unicorn, addrs+0x1c, &phoffset, sizeof(phoffset));
				uint32_t addr = addrs + phoffset;
				for(uint32_t i = 0; i < phsize; ++i){
					uint32_t val;
					uc_mem_read(this->unicorn, addr, &val, sizeof(val));
					if(val == 1){
						uc_mem_read(this->unicorn, addr + 4, &val, sizeof(val));
						f.seekp(val);
						uc_mem_read(this->unicorn, addr + 8, &val, sizeof(val));
						uint32_t size;
						uc_mem_read(this->unicorn, addr+ 0x10, &size, sizeof(size));
						auto data = std::make_unique<char[]>(size);
						uc_mem_read(this->unicorn, val, data.get(), size);
						f.write(data.get(), size);
					}
					addr += reader->GetPhsize();
				}
				f.close();
				std::cout << "!!!! OK !!!!\n";
				
			}				

			static void hook_syscall(uc_engine *uc,uint32_t number,  void *user_data){
    			uint32_t sys_num;
			    uc_reg_read(uc, UC_X86_REG_EAX, &sys_num);
				uint32_t arg1, arg2, arg3, arg4, ret;	
			    uc_reg_read(uc, UC_X86_REG_EBX, &arg1);
			    uc_reg_read(uc, UC_X86_REG_ECX, &arg2);
			    uc_reg_read(uc, UC_X86_REG_EDX, &arg3);
				x86Emulator *ptr = static_cast<x86Emulator *>(user_data);
				switch(sys_num){
					case 1: {
						std::cout << "SYS_EXIT|" << arg1 << "\n";
						uc_emu_stop(uc);
						break;
					}
					case 4: {
						std::cout << "SYS_WRITE|" << arg1 << "|" << arg2 << "|" << arg3 << "\n";
						break;
					}
					case 0x2d: {
						std::cout <<"SYS_BRK|" << arg1 << "\n";
						ptr->Dump();
						uc_emu_stop(uc);
						break;
					}
					case 0x55: {
						std::cout << "SYS_READLINK|" <<arg1 << "|" << arg2 << "|" << arg3 <<"\n";
						ret =42;
						uc_reg_write(uc, UC_X86_REG_EAX, &ret);
						break;
					}
					case 0x5a:{
						uint32_t addr, size, prot;
						uc_mem_read(uc, arg1, &addr, sizeof(addr));
						uc_mem_read(uc, arg1+4, &size, sizeof(size));
						uc_mem_read(uc, arg1+8, &prot, sizeof(prot));
						std::cout << "SYS_MMAP|" <<addr << "|" <<size<< "|"<<prot<< "\n";
						uc_err err = uc_mem_map(uc, addr, ptr->getAligned(size), prot);
						if(err){
							size_t x;
							std::cerr << ">>>>>>> " << uc_strerror(err) << "\n";
							err = uc_mem_unmap(uc, addr, size);
							if(err) {
								std::cerr << ">>>>>>> Unmmap: " << uc_strerror(err) << "\n";
								auto pair_mmap = std::find_if(ptr->maps.cbegin(), ptr->maps.cend(), [addr,size](std::pair<uint32_t, uint32_t> x){
										return (((x.first < addr) && (x.first+x.second > addr))||((x.first > addr)&&(x.first < addr + size))); 
									});
								if(pair_mmap != ptr->maps.cend()){
									err = uc_mem_unmap(uc, pair_mmap->first, pair_mmap->second);
									std::cerr << ">>>>>>> Unmmap2: " << uc_strerror(err) << "\n";
								}
							}
							err = uc_mem_map(uc, addr, ptr->getAligned(size), prot);		
							std::cerr << ">>>>>>> Mmap: " << uc_strerror(err) << "\n";
						}
						if(!err){
							ptr->maps.push_back(std::pair<uint32_t, uint32_t>(addr, ptr->getAligned(size)));
							uc_reg_write(uc, UC_X86_REG_EAX, &addr);
						} 
						break;	
					}
					case 0x5b: {
						std::cout << "SYS_UNMMAP|" << arg1 << "|" << arg2 << "\n";
						uc_err err = uc_mem_unmap(uc, arg1, arg2);
						if(err){
							std::cerr << uc_strerror(err);
						}else{
							std::remove_if(ptr->maps.begin(),ptr->maps.end(), [&arg1](std::pair<uint32_t, uint32_t> x){return x.first == arg1;});
						}
						uc_mem_region *reg;
						break;
					}
					case 0x7a: {std::cout << "SYS_NEWUNAME|" <<arg1 << "\n"; break;}
					case 0x7d: {
						std::cout << "SYS_MPROTECT|" <<arg1 << "|" <<arg2 << "|" << arg3 <<"\n";
						ret = 0;
						if(!ptr->addrs)
							ptr->addrs = arg1;
						uc_reg_write(uc, UC_X86_REG_EAX, &ret);
						/*auto data = std::make_unique<char[]>(arg2);
						uc_mem_read(uc, arg1, data.get(), arg2);
						ptr->f.write(data.get(), arg2);
						std::cout << ptr->f.tellp() << std::endl;
						*/
						break;	
					}							
					default: {
						std::cout << "Got syscall: " << sys_num << "\n";
						std::cout << "Now I'm exit\n";
						uc_emu_stop(uc);
					}
				}
			}	

			x86Emulator(std::shared_ptr<ELF::ElfReader<uint32_t>> r):ElfEmulator<uint32_t>(r){};
	
			void SetSyscallHook() override{
				uc_hook trace1;
				uc_hook_add(this->unicorn, &trace1, UC_HOOK_INTR, (void *)hook_syscall, this, 0, 0, UC_X86_INS_INT);
			}

			void SetTraceHook() override{
			}

			uint32_t GetAddr() override{
				uint32_t addr;
				uc_reg_read(this->unicorn, UC_X86_REG_EIP, &addr);
				return addr;
			}
	
		protected:
			uc_arch getArch() override{
				return UC_ARCH_X86;
			}

			uc_mode getSize() override{
				return (uc_mode)4;
			}

			uint32_t getAligned(uint32_t size) override{
				if (size & 0xfff){
					size = ((size >> 12)+1) << 12;
				}
				return size;
			}

			uint32_t getLowAlign(uint32_t addr) override{
				return (addr >> 16)	<< 16;
			}
			
			void setStackPointer() override{
				uint32_t addr = 0xc0000000;
				uint32_t size = 20 * 1024 * 1024;
				uint32_t initial = addr - 10*1024*1024;
				uc_mem_map(unicorn, addr-size, size, UC_PROT_READ | UC_PROT_WRITE);
				uc_reg_write(this->unicorn, UC_X86_REG_ESP, &initial);
			}

			void makeAux() override{
				uint32_t addr;
				uc_reg_read(this->unicorn, UC_X86_REG_ESP, &addr);
				uint32_t val = 0;
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));	
				addr += sizeof(val);
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));	
				addr += sizeof(val);
				val = 5;
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));	
				addr += sizeof(val);
				val = this->reader->GetPhnum();
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));	
				addr += sizeof(val);
				val = 3;	
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));	
				addr += sizeof(val);
				val = this->loadaddr + this->reader->GetPhoff();
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));	
				addr += sizeof(val);
				val = 9;	
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));	
				addr += sizeof(val);
				val = this->reader->GetEntry();
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));	
				addr += sizeof(val);
				val = 4;	
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));	
				addr += sizeof(val);
				val = this->reader->GetPhsize();
				uc_mem_write(this->unicorn, addr, &val, sizeof(val));	
				addr += sizeof(val);
			}
		public:

			void SetAddr() override{
				uint32_t addr = this->reader->GetEntry();
				uc_reg_write(this->unicorn, UC_X86_REG_EIP, &addr);
			}
	
			void PrintCPUCont() override{
				int32_t reg;
			    uc_reg_read(this->unicorn, UC_X86_REG_EAX, &reg);
    			std::cout << ">>> RAX = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_X86_REG_EBX, &reg);
    			std::cout << ">>> RBX = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_X86_REG_ECX, &reg);
    			std::cout << ">>> RCX = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_X86_REG_EDX, &reg);
    			std::cout << ">>> RDX = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_X86_REG_ESI, &reg);
    			std::cout << ">>> RSI = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_X86_REG_EDI, &reg);
    			std::cout << ">>> RDI = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_X86_REG_ESP, &reg);
    			std::cout << ">>> RSP = "<< reg << "\n";
			    uc_reg_read(this->unicorn, UC_X86_REG_EIP, &reg);
    			std::cout << ">>> RIP = "<< reg << "\n";
			}
	};
}

#endif
