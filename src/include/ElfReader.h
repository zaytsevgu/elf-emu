#ifndef ELF_LOADER_H
#define ELF_LOADER_H
#include <algorithm>
#include <string.h>
#include <array>
#include <iostream>
#include <fstream>
#include <type_traits>
#include <vector>
#include <string>
#include <memory>
#include <cassert>

#include "endian.h"

namespace ELF {

	template <typename T>
	struct  ElfData{
		std::unique_ptr<uint8_t []> ptr;
		size_t size;
		T      addr;	
	};

	template <typename T>
	struct Patch{
		ElfData<T> code;
		std::string section;
		size_t add;		
	};
	
	template <typename PTR>
	struct ElfSegment{
		uint32_t p_type;
		uint32_t p_flags;
		PTR      p_offset;
		PTR      p_vaddr;
		PTR      p_paddr;
		PTR      p_filesz;
		PTR      p_memsz;
		PTR      p_align;			
	};

	template <typename PTR>
	struct ElfSection{
		uint32_t sh_name;
		uint32_t sh_type;
		PTR      sh_flags;
		PTR      sh_addr;
		PTR      sh_offset;
		PTR      sh_size;
		uint32_t sh_link;
		uint32_t sh_info;
		PTR      sh_addralign;
		PTR      sh_entsize;
		std::string name;		
	};


	enum class P_TYPE:uint32_t {PT_NULL = 0, PT_LOAD};

	//this may be a good idea to restrict instantiation only with uint32_t and uint64_t
	//but if someone want to use int or long long?
	//template <typename PTR, typename = std::enable_if_t<std::is_same<uint64_t, PTR>::value>> 

	template <typename PTR>
	class ElfReader {

		public:

			ElfReader(const char * filename):input(filename, std::ios::binary){};
			ElfReader(ElfReader& a) = delete;
			ElfReader(ElfReader&& a) = delete;

			~ElfReader() = default;

			void LoadFile(bool load_sections = true){
				input.read(ident.data(), 16);
				readIntegerField(&e_type);
				readIntegerField(&e_machine);
				readIntegerField(&e_version);
				readIntegerField(&e_entry);
				readIntegerField(&e_phoff);
				readIntegerField(&e_shoff);
				readIntegerField(&e_flags);
				readIntegerField(&e_ehsize);
				readIntegerField(&e_phentsize);
				readIntegerField(&e_phnum);
				readIntegerField(&e_shentsize);
				readIntegerField(&e_shnum);
				readIntegerField(&e_shstrndx);
				if(e_phnum != 0){
					loadSegments();
				}
				if(load_sections){
					loadSections();
					loadSectionNames();
				}
				return;
			}

			PTR GetEntry(){
				return e_entry;
			}

			PTR GetMachine(){
				return e_machine;
			}

			uint8_t isBE(){
				return ident[5] == 2;
			}

			decltype(auto) GetSectionContent(const ElfSection<PTR> &sec){
				ElfData<PTR> retval;
				retval.ptr = std::make_unique<uint8_t[]>(sec.sh_size);
				retval.size = sec.sh_size;
				retval.addr = sec.sh_addr;
				input.seekg(sec.sh_offset, std::ios_base::beg);	
				input.read(retval.ptr.get(), retval.size); //array specification doesn't include operator *
				return retval;
			}

			decltype(auto) GetSegmentContent(const ElfSegment<PTR> &seg){
				ElfData<PTR> retval;
				retval.ptr = std::make_unique<uint8_t[]>(seg.p_memsz);
				retval.size = seg.p_memsz;
				retval.addr = seg.p_vaddr;
				input.seekg(seg.p_offset, std::ios_base::beg);
				input.read(reinterpret_cast<char *>(retval.ptr.get()), seg.p_filesz);
				return retval;
			}
	
			decltype(auto) GetSectionsIterator(){
				return sections.cbegin();
			}

			decltype(auto) GetSectionsEnd(){
				return sections.cend();
			}

			decltype(auto) GetSegmentsIterator(){
				return segments.cbegin();
			}

			decltype(auto) GetSegmentsEnd(){
				return segments.cend();
			}
			
			PTR GetPhoff(){
				return e_phoff;
			}
			PTR GetPhnum(){
				return e_phnum;
			}
			PTR GetPhsize(){
				return e_phentsize;
			}
		private:
		
			//Well this is more beauty than using a bunch of
			//reintepret cast everyewhere
			//also here is check that we passed pointer to number,
			//so sizeof(a) must be that we expected
			template<typename T>
			void readIntegerField(T *a){
				readIntegerFieldInner<T>(a, std::is_integral<T>());
				if(ident[5] == 2){
					*a = getBE(a); 
				}
			}


			template<typename T>
			T getBE(T *a){ //Well, it won't work on BE platfroms
				if(sizeof(T) == 4)
					return htobe32(*a);
				if(sizeof(T) == 8)
					return htobe64(*a);
				if(sizeof(T) == 2)
					return htobe16(*a);
			}

			template<typename T>
			void readIntegerFieldInner(T *a, std::true_type){
				input.read(reinterpret_cast<char *>(a), sizeof(T));
			}

			template<typename T>
			void writeIntegerField(std::ofstream &b, T *a){
				writeIntegerFieldInner<T>(b, a, std::is_integral<T>());
			}

			template<typename T>
			void writeIntegerFieldInner(std::ofstream &b, T *a, std::true_type){
				b.write(reinterpret_cast<char *>(a), sizeof(T));
			}
	
			void loadSegments(){
				input.seekg(e_phoff, std::ios_base::beg);
				for(int i=0; i < e_phnum; ++i){
					readSegment(std::conditional_t<sizeof(PTR) == 8, std::true_type, std::false_type>());
				}
			}

			//x64 segment
			void readSegment(std::true_type){
				ElfSegment<PTR> segm;
				//std::cout << "x64" << std::endl;
				readIntegerField(&segm.p_type);
				readIntegerField(&segm.p_flags);
				readIntegerField(&segm.p_offset);
				readIntegerField(&segm.p_vaddr);
				readIntegerField(&segm.p_paddr);
				readIntegerField(&segm.p_filesz);
				readIntegerField(&segm.p_memsz);
				readIntegerField(&segm.p_align);
				segments.push_back(segm);
			}

			//x86 segment
			void readSegment(std::false_type){
				//std::cout << "x32" << std::endl;
				ElfSegment<PTR> segm;
				readIntegerField(&segm.p_type);
				readIntegerField(&segm.p_offset);
				readIntegerField(&segm.p_vaddr);
				readIntegerField(&segm.p_paddr);
				readIntegerField(&segm.p_filesz);
				readIntegerField(&segm.p_memsz);
				readIntegerField(&segm.p_flags);
				readIntegerField(&segm.p_align);
				segments.push_back(segm);
			}

			void loadSection(){
				ElfSection<PTR> sec;
				readIntegerField(&sec.sh_name);
				readIntegerField(&sec.sh_type);
				readIntegerField(&sec.sh_flags);
				readIntegerField(&sec.sh_addr);
				readIntegerField(&sec.sh_offset);
				readIntegerField(&sec.sh_size);
				readIntegerField(&sec.sh_link);
				readIntegerField(&sec.sh_info);
				readIntegerField(&sec.sh_addralign);
				readIntegerField(&sec.sh_entsize);
				sections.push_back(sec);
			}

			void loadSections(){
				input.seekg(e_shoff, std::ios_base::beg);
				for(int i=0; i < e_shnum; ++i){
					loadSection();
				}
				return;
			}

			void loadSectionNames(){
				auto strIndex = sections[e_shstrndx];
				for(auto& sec : sections){
					input.seekg(strIndex.sh_offset + sec.sh_name, std::ios_base::beg);
					std::getline(input, sec.name, '\x00');
				}
			}

			std::ifstream input;
			//HEADER
			std::array<char,16> ident;
			uint16_t e_type;
			uint16_t e_machine;
			uint32_t e_version;
			PTR      e_entry;
			PTR      e_phoff;
			PTR      e_shoff;
			uint32_t e_flags;
			uint16_t e_ehsize;
			uint16_t e_phentsize;
			uint16_t e_phnum;
			uint16_t e_shentsize;
			uint16_t e_shnum;
			uint16_t e_shstrndx;
			//SEGMENTS
			std::vector<ElfSegment<PTR>> segments;	
			//SECTIONS
			std::vector<ElfSection<PTR>> sections;
	};
} //end namespace
#endif
