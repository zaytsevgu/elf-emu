#ifndef ELF_EMU_g

#define ELF_EMU_g


#include <iomanip>
#include <iterator>
#include <algorithm>
#include "ElfReader.h"
#include <unicorn/unicorn.h>

namespace EMU{
	

	template<typename T>
	class ElfEmulator {
		
		public:
			ElfEmulator(std::shared_ptr<ELF::ElfReader<T>> r):reader(r), loadaddr(0), addrs(0){
			}


			virtual ~ElfEmulator(){
				uc_close(unicorn);			
			}

			bool Load(){
				uc_err err = uc_open(getArch(), getSize(), &unicorn);
				if(err){
					std::cerr << "Error opening emulator\n";
					return false;
				}
				auto it = reader->GetSegmentsIterator();
				auto end = reader->GetSegmentsEnd();
				for (;it != end; ++it){
					if(it->p_type == 1){
						T addr = it->p_vaddr;
						T mmaped_addr = getLowAlign(addr);
						T size = getAligned(it->p_memsz + addr - mmaped_addr);
						uc_err err = uc_mem_map(unicorn, mmaped_addr, size, it->p_flags);
						if(err){
							std::cerr << "Error mmaping " << uc_strerror(err) << "\n";
						}else{
							std::cout << "Mmaped: " << std::hex << mmaped_addr << " " <<  size << "\n";
							if(!loadaddr){
								loadaddr = mmaped_addr;
							}
							maps.push_back(std::pair<T,T>(mmaped_addr, size));
						}
						ELF::ElfData<T> data = reader->GetSegmentContent(*it); 
						err = uc_mem_write(unicorn, data.addr, data.ptr.get(), data.size);
						if(err){
							std::cerr << "Error writing segment into memory:" <<uc_strerror(err) << "\n";
							std::cerr << data.addr << " " << data.size << "\n";
							return false;
						}
					}
					setStackPointer();
					makeAux();
				}
				return true;
			}

			virtual void SetAddr() = 0;
			virtual T    GetAddr() = 0;	
			virtual void SetTraceHook() = 0;
			virtual void SetSyscallHook() = 0;
			virtual void Dump() = 0;
			
			bool MakeStep(){
				uint64_t addr;	
			    uc_reg_read(this->unicorn, UC_X86_REG_EIP, &addr);
				uc_err err = uc_emu_start(unicorn, addr, 0, 0, 1);
				if(err){
					std::cerr << "Failed on uc_emu_start() with error returned "<<err<<" :" << uc_strerror(err) << "\n";
					return false;
				}
				
				return true;
			}

			bool Run(){
				uc_err err = uc_emu_start(unicorn, GetAddr(), 0, 0, 0);
				if(err){
					std::cerr << "Failed on uc_emu_start() with error returned "<<err<<" :" << uc_strerror(err) << "\n";
				}
				return true;
			}
			
			virtual void PrintCPUCont() = 0;

		protected:

			virtual uc_arch  getArch() = 0;
			virtual uc_mode  getSize() = 0;
			virtual T        getAligned(T size) = 0;
			virtual T        getLowAlign(T addr) = 0;
			virtual void     setStackPointer() = 0;
			virtual void     makeAux() = 0;

			std::shared_ptr<ELF::ElfReader<T>> reader;
			uc_engine *                        unicorn;
			std::vector<std::pair<T,T>>    	   maps;
			T                                  loadaddr;	
			T                                  addrs;
	};

}

#endif
