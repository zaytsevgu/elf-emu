#ifndef EMULA

#define EMULA

#include "ElfEmulator.h"
#include "x64Emulator.h"
#include "x86Emulator.h"
#include "armEmulator.h"
#include "mipsEmulator.h"


std::shared_ptr<EMU::ElfEmulator<uint64_t>> get64Emu(std::shared_ptr<ELF::ElfReader<uint64_t>> rd){
	rd->LoadFile(false);
	switch(rd->GetMachine()){
		case 0x3e:
			std::cout << "Got x86_64" << std::endl;
			return std::make_shared<EMU::x64Emulator>(rd);
			break;
		default:
			exit(1);
	}
}

std::shared_ptr<EMU::ElfEmulator<uint32_t>> get32Emu(std::shared_ptr<ELF::ElfReader<uint32_t>> rd){
	rd->LoadFile(false);
	switch(rd->GetMachine()){
		case 0x3:
			std::cout << "Got x86" << std::endl;
			return std::make_shared<EMU::x86Emulator>(rd);
		case 0x8:
			std::cout << "Got MIPS" << std::endl;
			return std::make_shared<EMU::mipsEmulator>(rd);
		case 0x28:
			std::cout << "Got ARM" << std::endl;
			return std::make_shared<EMU::armEmulator>(rd);
		default:
			exit(1);
	}
}
#endif
