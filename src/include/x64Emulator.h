#ifndef ELF_EMU_g64

#define ELF_EMU_g64

#include <iomanip>
#include "ElfReader.h"
#include "ElfEmulator.h"
#include <unicorn/unicorn.h>


namespace EMU{

	class x64Emulator: public ElfEmulator<uint64_t> {
		public:
			x64Emulator(std::shared_ptr<ELF::ElfReader<uint64_t>> r):ElfEmulator<uint64_t>(r){};
		
		protected:
			
			static void trace_hook(uc_engine *uc, uint64_t address, uint32_t size, void *user_data){
							int eflags;
							std::cout << "Tracing instruction at " << address <<" instruction size = "<< size << "\n";
							uc_reg_read(uc, UC_X86_REG_EFLAGS, &eflags);
							std::cout << " --- EFLAGS is " << eflags << "\n";
			}

			static void entry_get_hook(uc_engine *uc, uc_mem_type type,
						uint64_t address, int size, int64_t value, void *user_data) {
				switch(type) {
					default: break;
					case UC_MEM_WRITE:
						std::cout << "Address " << address << " got Entry: " <<  value <<"\n";
						break;
				}
			}


			void Dump() override{
				/**
                 * UGLY DUMPER
                 **/
				std::cout << "!!!! RUNNING UGLY DUMPER !!!!\n";
                std::ofstream f("out_dump");
                uint16_t phsize;
                uint64_t phoffset;
                uc_mem_read(this->unicorn, this->addrs+0x38, &phsize, sizeof(phsize));
                uc_mem_read(this->unicorn, addrs+0x20, &phoffset, sizeof(phoffset));
                uint32_t addr = addrs + phoffset;
                for(uint32_t i = 0; i < phsize; ++i){
					uint64_t val;
					uint32_t type;
					uc_mem_read(this->unicorn, addr, &type, sizeof(type));
                    if(type == 1){ 
                    	uc_mem_read(this->unicorn, addr + 8, &val, sizeof(val));
                        f.seekp(val);
                        uc_mem_read(this->unicorn, addr + 0x10, &val, sizeof(val));
                        uint32_t size;
                        uc_mem_read(this->unicorn, addr+ 0x20, &size, sizeof(size));
                        auto data = std::make_unique<char[]>(size);
                        uc_mem_read(this->unicorn, val, data.get(), size);
                        f.write(data.get(), size);
                    }   
                 	addr += reader->GetPhsize();
                }   
                f.close();
                std::cout << "!!!! OK !!!!\n";
			}

			static void syscall_hook(uc_engine *uc, void *user_data)
			{
				uint64_t syscall_num;
				uint64_t arg1;
				uint64_t arg2;
				uint64_t arg3;
				uint64_t arg4;
				uint64_t ret = 42;
				x64Emulator *ptr = static_cast<x64Emulator *>(user_data);
				uc_reg_read(uc, UC_X86_REG_RAX, &syscall_num);
				uc_reg_read(uc, UC_X86_REG_RDI, &arg1);
				uc_reg_read(uc, UC_X86_REG_RSI, &arg2);
				uc_reg_read(uc, UC_X86_REG_RDX, &arg3);
				uc_reg_read(uc, UC_X86_REG_RCX, &arg4);
				switch (syscall_num){
					case 0x0: {
							std::cout << "SYS_READ|" << arg1 << "|" << arg2 << "|" << arg3 << "\n";
					}
					case 0x1: {
							std::cout << std::hex << "SYS_WRITE|" << arg1 << "|" << arg2 << "|" <<arg3 << "\n";
							break;
						}
					case 0x9: {
							std::cout << std::hex << "SYS_MMAP|" << arg1 <<"|" << arg2 <<"|" <<arg3 <<"|" << arg4 << "\n";
							uc_err err = uc_mem_map(uc, arg1, ptr->getAligned(arg2), 7);
							if(err){
								err = uc_mem_unmap(uc, arg1, arg2);
                            	if(err) {
                                	std::cerr << ">>>>>>> Unmmap: " << uc_strerror(err) << "\n";
									auto pair_mmap = ptr->maps.cbegin();
									do{
                                		pair_mmap = std::find_if(ptr->maps.cbegin(), ptr->maps.cend(), [arg1,arg2](std::pair<uint32_t, uint32_t> x){
                                        	return (((x.first <= arg1) && (x.first+x.second > arg1))||((x.first >= arg1)&&(x.first < arg1 + arg2)));
                                    	});
                                		if(pair_mmap != ptr->maps.cend()){
											std::cout << pair_mmap->first << " " << pair_mmap->second << "\n";
                                    		err = uc_mem_unmap(uc, pair_mmap->first, pair_mmap->second);
                                    		std::cerr << ">>>>>>> Unmmap2: " << uc_strerror(err) << "\n";
											ptr->maps.erase(pair_mmap);
                                		}else{
											break;
										}
									}while(!err);
                            	}
                            	err = uc_mem_map(uc, arg1, ptr->getAligned(arg2), arg3);
                            	std::cerr << ">>>>>>> Mmap: " << uc_strerror(err) << "\n";
							}
							if(!err){
        	                    ptr->maps.push_back(std::pair<uint32_t, uint32_t>(arg1, ptr->getAligned(arg2)));
            	                uc_reg_write(uc, UC_X86_REG_EAX, &arg1);
                	        }else{
								ret = -2;	
            	                uc_reg_write(uc, UC_X86_REG_EAX, &ret);
							}
							break;
					}
					case 0x2: {
						std::cout << "SYS_OPEN|" << arg1 << "|" << arg2 << "|" <<arg3 << "\n";
						ptr->Dump(); //if we here - UPX start to open linker
						break;
					}
					case 0xa:{
							std::cout << "SYS_MPROTECT|" << arg1 <<"|" << arg2 <<"|" <<arg3 <<"|" << arg4 << "\n";
							ret = 0;
							if(!ptr->addrs)
								ptr->addrs = arg1;
							uc_reg_write(uc, UC_X86_REG_RAX, &ret);
							break;
					}
					case 0x3c: {	
							std::cout << "SYS_EXIT|" << arg1 << "\n";
							uc_emu_stop(uc);
							break;
						}
					case 0x59: {
							std::cout << "SYS_READLINK|" << arg1 << "|" << arg2 << "|" << arg3 << "\n";
							char name[256];
							uc_mem_read(uc, arg1, name, sizeof(name));
							ret = 0x42;
							uc_reg_write(uc, UC_X86_REG_RAX, &ret);
							std::cout << "Reading " << name << "\n";
							break;
							//std::cout << "Dumping " << size << " " << addr << "\n";
							//auto data = std::make_unique<char[]>(size);
							//uc_err err = uc_mem_read(uc, addr, data.get(), size);
							//std::ofstream f("out", std::ios_base::binary);
							//f.write(data.get(), size); 
						}
					default: {
							std::cout << "Got "<< syscall_num<< " syscall\n";
							std::cout << "Now I'm exit\n";
							uc_emu_stop(uc);
						}
				}
			}

			uc_arch getArch() override{
				return UC_ARCH_X86;
			}

			uc_mode getSize() override{
				return (uc_mode)8;
			}

			uint64_t getAligned(uint64_t size) override{
				if (size & 0xfff){
					size = ((size >> 12)+1) << 12;
				}
				return size;
			}

			uint64_t getLowAlign(uint64_t addr) override{
				return (addr >> 16)	<< 16;
			}
			
			void setStackPointer() override{
				uint64_t addr = 0x7fff00000000;
				uint64_t size = 200 * 1024 * 1024;
				uc_mem_map(this->unicorn, addr-size, size, UC_PROT_READ | UC_PROT_WRITE);
				uint64_t initial_stack =  addr- 100 * 1024 * 1024;
				uc_reg_write(this->unicorn, UC_X86_REG_RSP, &initial_stack);
			}

			void makeAux() override{
				//T aux = 0x7ffe000000000;
				//T size = 1024;
				//uc_mem_map(this->unicorn, aux, size, UC_PROT_READ|UC_PROT_WRITE);
				uint64_t rsp;
				uint64_t zero = 0;
				uint64_t one = 1;
				uint64_t three = 3;
				uint64_t five = 5;
				uc_reg_read(this->unicorn, UC_X86_REG_RSP, &rsp);
				uc_mem_write(this->unicorn, rsp, &one, 8); //argc
				uc_mem_write(this->unicorn, rsp+8, &zero, 8); //envp
				uc_mem_write(this->unicorn, rsp+16, &zero, 8); //envp
				uc_mem_write(this->unicorn, rsp+24, &zero, 8); //envp
				uc_mem_write(this->unicorn, rsp+32, &three, 8); //envp
				uint64_t pheaders = this->loadaddr + this->reader->GetPhoff();
				uc_mem_write(this->unicorn, rsp+40, &pheaders, 8);
				uc_mem_write(this->unicorn, rsp+48, &five, 8);
				pheaders = this->reader->GetPhnum();
				uc_mem_write(this->unicorn, rsp+56, &pheaders, 8);
				pheaders = 9;
				uc_mem_write(this->unicorn, rsp+64, &pheaders, 8);
				pheaders = this->reader->GetEntry();
				uc_mem_write(this->unicorn, rsp+72, &pheaders, 8);

			}

		public:

			void SetSyscallHook() override{
				uc_hook hook;
				uc_hook_add(this->unicorn, &hook, UC_HOOK_INSN, (void *)syscall_hook, this, 1, 0, UC_X86_INS_SYSCALL);
			}

			void SetTraceHook() override{
				uc_hook trace;
				uc_hook_add(this->unicorn, &trace, UC_HOOK_CODE, (void *)trace_hook, NULL, 1, 0);
			}

			void SetAddr() override{
				uint64_t addr = this->reader->GetEntry();
				uc_reg_write(this->unicorn, UC_X86_REG_RIP, &addr);
			}
			
			uint64_t GetAddr() override{
				uint64_t ret;
				uc_reg_read(this->unicorn, UC_X86_REG_RIP, &ret);
				return ret;
			}	
			void PrintCPUCont() override{
				int64_t rax;
				int64_t rbx;
				int64_t rcx;
				int64_t rdx;
				int64_t rsi;
				int64_t rdi;
				int64_t r8;
				int64_t r9;
				int64_t r10;
				int64_t r11;
				int64_t r12;
				int64_t r13;
				int64_t rsp;
				int64_t rip;
			    uc_reg_read(this->unicorn, UC_X86_REG_RAX, &rax);
			    uc_reg_read(this->unicorn, UC_X86_REG_RBX, &rbx);
			    uc_reg_read(this->unicorn, UC_X86_REG_RCX, &rcx);
			    uc_reg_read(this->unicorn, UC_X86_REG_RDX, &rdx);
			    uc_reg_read(this->unicorn, UC_X86_REG_RSI, &rsi);
			    uc_reg_read(this->unicorn, UC_X86_REG_RDI, &rdi);
			    uc_reg_read(this->unicorn, UC_X86_REG_R8, &r8);
			    uc_reg_read(this->unicorn, UC_X86_REG_R9, &r9);
			    uc_reg_read(this->unicorn, UC_X86_REG_R10, &r10);
			    uc_reg_read(this->unicorn, UC_X86_REG_R11, &r11);
			    uc_reg_read(this->unicorn, UC_X86_REG_R12, &r12);
			    uc_reg_read(this->unicorn, UC_X86_REG_R13, &r13);
			    uc_reg_read(this->unicorn, UC_X86_REG_RSP, &rsp);
			    uc_reg_read(this->unicorn, UC_X86_REG_RIP, &rip);

    			std::cout << ">>> RAX = "<< rax << "\n";
    			std::cout << ">>> RBX = "<< rbx << "\n";
    			std::cout << ">>> RCX = "<< rcx << "\n";
    			std::cout << ">>> RDX = "<< rdx << "\n";
    			std::cout << ">>> RSI = "<< rsi << "\n";
    			std::cout << ">>> RDI = "<< rdi << "\n";
    			std::cout << ">>> R8 = "<< r8 << "\n";
    			std::cout << ">>> R9 = "<< r9 << "\n";
    			std::cout << ">>> R10 = "<< r10 << "\n";
    			std::cout << ">>> R11 = "<< r11 << "\n";
    			std::cout << ">>> R12 = "<< r12 << "\n";
    			std::cout << ">>> R13 = "<< r13 << "\n";
    			std::cout << ">>> RSP = "<< rsp << "\n";
    			std::cout << ">>> RIP = "<< rip << "\n";
			}
	};
}

#endif
